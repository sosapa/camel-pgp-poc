# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Proof of concept of the use of Camel PGP
* [Based on PGP enc/dec with apache camel](https://dzone.com/articles/pgp-encryption-and-decryption-with-apache-camel)
* [Apache camel Crypto Documentation](http://camel.apache.org/crypto.html)
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

  [Followed the following for PGP setup](https://stackoverflow.com/questions/5587513/how-to-export-private-secret-asc-key-to-decrypt-gpg-files-in-windows)
  * Install pgp if not in place

        brew install gpg

  * Generate PGP keys

        gpg --gen-key use all defaults
        
  * Get the key user id
     
        gpg --list-keys

  * Export PGP public key

         gpg --output pgppubkey.pgp --export "Pablo Sosa <pablo.sosa@redenergy.com.au>"
 
  * Export PGP private key

        gpg --output pgpprivkey.pgp --export-secret-keys "Pablo Sosa <pablo.sosa@redenergy.com.au>"

  * runs the application that reads from fromSource and writes encrypted message to folder toTarget 

        gw clean bootRun 

* Configuration
Create a simple file on the source folder
* Dependencies
apache camel
bouncycastle 
* How to run tests
TODO
* Deployment instructions
JUST A POC not intended to be deployed. That said is a simple springboot app.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact