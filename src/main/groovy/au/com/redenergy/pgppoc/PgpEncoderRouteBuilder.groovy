package au.com.redenergy.pgppoc

import org.apache.camel.builder.RouteBuilder
import org.springframework.stereotype.Component

@Component(value = "pgpPoc")
class PgpEncoderRouteBuilder extends RouteBuilder {

  @Override
  void configure() throws Exception {
    log.info("Starting route config")
    String keyUserid = "Pablo Sosa <pablo.sosa@redenergy.com.au>"
    String publicKeyFileName = "classpath:/pgppubkey.pgp"

    final String secretKeyFileName = "classpath:/pgpprivkey.pgp"
    final String keyPassword = "totally secret"

    from("file://./fromSource?delay=5000")
        .autoStartup(true)
        .routeId("source")
        .log("Received file [\${file:name}], proceed to move to encrypted folder")
        .marshal()
        .pgp(publicKeyFileName, keyUserid)
        .to("file://./toTarget")

    from("file://./toTarget?delay=15000")
        .autoStartup(true)
        .routeId("decrypt")
        .log("Encrypted file [\${file:name}], proceed to move to decrypted folder]")
        .unmarshal()
        .pgp(secretKeyFileName, keyUserid, keyPassword)
        .to("file://./toDecrypted")
  }
}